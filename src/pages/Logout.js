import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);

  useEffect(() => {
    // Clear the localStorage
    unsetUser();

    // Update the user context with null
    setUser({ id: null });
  }, [unsetUser, setUser]);

  // Redirect back to login
  return <Navigate to="/login" />;
}

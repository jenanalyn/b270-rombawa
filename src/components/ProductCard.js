import React from 'react';
import { Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import Gun from '../productImages/Gun.png';
import Katana from '../productImages/Katana.png';
import Knife from '../productImages/Knife.png';
import Machete from '../productImages/Machete.png';

export default function ProductCard({ productProp }) {
  const { _id, name, description, price } = productProp;

  const productImages = {
    Gun,
    Katana,
    Knife,
    Machete,
  };

  const imagePath = productImages[name];

  return (
    <Row className="justify-content-center">
      <Col xs={12} md={6} lg={4}>
        <Card className="mb-4">
          <div className="image-wrapper">
            <img src={imagePath} alt="Product" className="product-image" />
          </div>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
            <Button variant="primary" as={Link} to={`/products/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
